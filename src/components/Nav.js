import React from "react";
import { NavLink } from "react-router-dom";

const Nav = () => {
	return (
		<div className="bg-purple-900 w-full box-border flex items-center justify-between px-10 py-6">
			<div className="text-purple-100 text-2xl font-bold">ReactModules</div>
			<div className="md:flex hidden items-center space-x-3 text-purple-300">
				<NavLink
					to="/"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					Home
				</NavLink>
				<NavLink
					to="/users"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					Users
				</NavLink>
				<NavLink
					to="/about"
					className={({ isActive }) => (isActive ? "active" : "inactive")}
				>
					About
				</NavLink>
			</div>
			<button className="text-base text-purple-900 bg-purple-100 px-8 py-4 rounded-full">
				Register
			</button>
		</div>
	);
};

export default Nav;
