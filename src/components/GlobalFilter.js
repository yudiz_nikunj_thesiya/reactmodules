import React, { useState } from "react";
import { FiSearch } from "react-icons/fi";
import { useAsyncDebounce } from "react-table";

const GlobalFilter = ({ filter, setFilter }) => {
	const [value, setValue] = useState(filter);

	const onChange = useAsyncDebounce((value) => {
		setFilter(value || undefined);
	}, 1000);
	return (
		<div className="flex w-full items-center space-x-3 border border-purple-700 hover:border-purple-400 px-4 py-4 rounded-2xl transition-colors duration-200 shadow-xl ease-in-out text-sm">
			<span className="text-purple-100">
				<FiSearch />
			</span>
			<input
				className="w-full bg-transparent text-purple-100 outline-none placeholder-purple-500"
				value={value || ""}
				placeholder="Search Employee"
				onChange={(e) => {
					setValue(e.target.value);
					onChange(e.target.value);
				}}
			/>
		</div>
	);
};

export default GlobalFilter;
