import React from "react";
import { FiSearch } from "react-icons/fi";

const ColumnFilter = ({ column }) => {
	const { filterValue, setFilter } = column;
	return (
		<div className="flex w-full items-center space-x-3 border border-purple-700 hover:border-purple-400 px-4 py-3 rounded-xl transition-colors duration-200  ease-in-out mt-3">
			<span className="text-purple-900">
				<FiSearch />
			</span>
			<input
				className="w-full bg-transparent text-purple-900 outline-none placeholder-purple-700 text-xs"
				value={filterValue || ""}
				placeholder="Search"
				onChange={(e) => setFilter(e.target.value)}
			/>
		</div>
	);
};

export default ColumnFilter;
