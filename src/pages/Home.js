import React from "react";
import Helmet from "react-helmet";
import { NavLink } from "react-router-dom";
import HeroSection from "../components/HeroSection";
import Nav from "../components/Nav";

const Home = () => {
	return (
		<>
			<Helmet>
				<meta charSet="utf-8" />
				<title>Home</title>
			</Helmet>
			<Nav />
			<HeroSection />
		</>
	);
};

export default Home;
